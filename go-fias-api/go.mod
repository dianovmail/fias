module fias/go-fias-api

require (
	github.com/elastic/go-elasticsearch v0.0.0
	github.com/gorilla/mux v1.7.4-0.20190720201435-e67b3c02c719
	github.com/rs/cors v1.7.0
)

go 1.12
