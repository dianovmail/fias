package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/elastic/go-elasticsearch"
	"github.com/gorilla/mux"
)

// Store allows to index and search documents.
//
type Store struct {
	es        *elasticsearch.Client
	indexName string
}

func getAdresses(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	store, _ := NewStore()
	w.Header().Set("Content-Type", "application/json")

	result, _ := store.Search(params["name"])
	json.NewEncoder(w).Encode(result)
}

// NewStore returns a new instance of the store.
//
func NewStore() (*Store, error) {
	cfg := elasticsearch.Config{
		Addresses: []string{
			"http://elasticsearch:9200",
		},
	}
	es, err := elasticsearch.NewClient(cfg)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	s := Store{es: es, indexName: "adress"}
	return &s, nil
}

// M is an alias for map[string]interface{}
type M map[string]interface{}

// Search returns results matching a query, paginated by after.
//
func (s *Store) Search(name string) ([]string, error) {

	var (
		buf      bytes.Buffer
		r        map[string]interface{}
		result   []string
		query    M
		sbCity   strings.Builder
		sbAdress strings.Builder
	)

	names := strings.Split(name, " ")
	if len(names) > 1 {
		query = M{
			"query": M{
				"bool": M{
					"must": []M{
						{
							"has_child": M{
								"type": "adress",
								"query": M{
									"match_bool_prefix": M{
										"name": names[1],
									},
								},
								"inner_hits": M{
									"size": "20",
								},
							},
						},
						{"match_bool_prefix": M{
							"name": names[0],
						}},
						{"match": M{
							"level": "1 2 3 4 5 6",
						}},
					},
				},
			},
		}
	} else {
		query = M{
			"query": M{
				"bool": M{
					"must": []M{
						{"match_bool_prefix": M{
							"name": names[0],
						}},
						{"match": M{
							"level": "1 2 3 4 5 6",
						}},
					},
				},
			},
		}
	}
	if err := json.NewEncoder(&buf).Encode(query); err != nil {
		log.Fatalf("Error encoding query: %s", err)
	}

	// Perform the search request.
	res, err := s.es.Search(
		s.es.Search.WithContext(context.Background()),
		s.es.Search.WithIndex(s.indexName),
		s.es.Search.WithBody(&buf),
		s.es.Search.WithTrackTotalHits(true),
		s.es.Search.WithPretty(),
	)
	if err != nil {
		log.Fatalf("Error getting response: %s", err)
	}
	defer res.Body.Close()

	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			log.Fatalf("Error parsing the response body: %s", err)
		} else {
			// Print the response status and error information.
			log.Fatalf("[%s] %s: %s",
				res.Status(),
				e["error"].(map[string]interface{})["type"],
				e["error"].(map[string]interface{})["reason"],
			)
		}
	}

	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		log.Fatalf("Error parsing the response body: %s", err)
	}
	// Print the response status, number of results, and request duration.
	log.Printf(
		"[%s] %d hits; took: %dms",
		res.Status(),
		int(r["hits"].(map[string]interface{})["total"].(map[string]interface{})["value"].(float64)),
		int(r["took"].(float64)),
	)
	// Format result.
	for _, hit := range r["hits"].(map[string]interface{})["hits"].([]interface{}) {
		//log.Printf(" * ID=%s, %s", hit.(map[string]interface{})["_id"], hit.(map[string]interface{})["_source"])
		//log.Printf(" * inner hits=%s", hit.(map[string]interface{})["inner_hits"])
		fmt.Fprintf(&sbCity, "%s", hit.(map[string]interface{})["_source"].(map[string]interface{})["shortname"])
		sbCity.WriteString(" ")
		fmt.Fprintf(&sbCity, "%s", hit.(map[string]interface{})["_source"].(map[string]interface{})["name"])
		innerHits := hit.(map[string]interface{})["inner_hits"]
		if innerHits != nil {
			for _, innerHit := range innerHits.(map[string]interface{})["adress"].(map[string]interface{})["hits"].(map[string]interface{})["hits"].([]interface{}) {
				sbAdress.WriteString(sbCity.String())
				sbAdress.WriteString(" ")
				fmt.Fprintf(&sbAdress, "%s", innerHit.(map[string]interface{})["_source"].(map[string]interface{})["shortname"])
				sbAdress.WriteString(" ")
				fmt.Fprintf(&sbAdress, "%s", innerHit.(map[string]interface{})["_source"].(map[string]interface{})["name"])
				result = append(result, sbAdress.String())
				sbAdress.Reset()
			}
		} else {
			result = append(result, sbCity.String())
		}
		//s := fmt.Sprintf("%s", hit.(map[string]interface{})["_source"])

		sbCity.Reset()
	}
	result = append(result, fmt.Sprintf(
		"[%s] %d hits; took: %dms",
		res.Status(),
		int(r["hits"].(map[string]interface{})["total"].(map[string]interface{})["value"].(float64)),
		int(r["took"].(float64)),
	))
	return result, nil
}
