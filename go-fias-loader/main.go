package main

import (
	"bufio"
	"log"
	"os"
	"path/filepath"
	"time"
)

// FileProperty struct for saving files properties
type FileProperty struct {
	FileInfo  os.FileInfo
	Processed bool
}

func main() {
	fileMap := make(map[string]*FileProperty)
	done := make(chan bool)
	// need all this shit because fsnotify does not work on mounted docker directories
	go func() {
		for {
			dirname := "/data"

			f, err := os.Open(dirname)
			if err != nil {
				log.Fatal(err)
			}
			files, err := f.Readdir(-1)
			f.Close()
			if err != nil {
				log.Fatal(err)
			}

			for _, file := range files {
				if !file.IsDir() {
					if f, ok := fileMap[file.Name()]; ok {
						// start processing file then his size stopped changing
						if file.Size() == f.FileInfo.Size() && file.ModTime() == f.FileInfo.ModTime() {
							if !f.Processed {
								f.Processed = true
								if err := processFile(filepath.Join(dirname, file.Name())); err != nil {
									log.Println(err)
								}
							}
						} else { // size has changed
							f.Processed = false
							f.FileInfo = file
						}
					} else { // new file
						fileMap[file.Name()] = &FileProperty{
							FileInfo:  file,
							Processed: false,
						}
					}
				}
			}
			time.Sleep(2 * time.Second)
		}
	}()
	log.Printf("Starting watcher for data")

	<-done

}

func processFile(fileName string) error {
	log.Printf("Processing %s", fileName)
	file, err := os.Open(fileName)
	if err != nil {
		return err
	}
	xmlReader := bufio.NewReader(file)

	reader, _ := ParseXML(xmlReader)

	BulkLoadToES(reader)
	return nil
}
