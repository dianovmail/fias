package main

import (
	"encoding/xml"
	"io"
)

// AdressObject represents a parse result
type AdressObject struct {
	ID           string `json:"id"`
	Name         string `json:"name"`
	Shortname    string `json:"shortname"`
	GUID         string `json:"guid"`
	Parent       string `json:"parent"`
	Level        string `json:"level"`
	RelationType struct {
		Name   string `json:"name"`
		Parent string `json:"parent,omitempty"`
	} `json:"relation_type"`
	Status string
}

//ParseXML processing xml files
func ParseXML(rd io.Reader) (<-chan AdressObject, error) {
	chnl := make(chan AdressObject)
	go func() {
		decoder := xml.NewDecoder(rd)
		for {
			tag, _ := decoder.Token()
			if tag == nil {
				break
			}
			switch se := tag.(type) {
			case xml.StartElement:
				if se.Name.Local == "Object" {
					object := AdressObject{}
					for _, attribute := range se.Attr {
						switch attribute.Name.Local {
						case "AOID":
							object.ID = attribute.Value
						case "FORMALNAME":
							object.Name = attribute.Value
						case "SHORTNAME":
							object.Shortname = attribute.Value
						case "AOGUID":
							object.GUID = attribute.Value
						case "PARENTGUID":
							object.Parent = attribute.Value
						case "AOLEVEL":
							object.Level = attribute.Value
						case "ACTSTATUS":
							object.Status = attribute.Value
						}
					}
					if object.Status == "1" {
						switch object.Level {
						case "7", "8", "9":
							object.RelationType.Name = "adress"
							object.RelationType.Parent = object.Parent
						default:
							object.RelationType.Name = "city"

						}
						chnl <- object
					}
				}
			case xml.EndElement:
				if se.Name.Local == "AddressObjects" {
					break
				}
			}

		}
		close(chnl)
	}()
	return chnl, nil
}
