package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"

	elasticsearch "github.com/elastic/go-elasticsearch/v7"
	esapi "github.com/elastic/go-elasticsearch/v7/esapi"
)

// Store allows to index and search documents.
//
type Store struct {
	es        *elasticsearch.Client
	indexName string
}

//LoadToES performs data loat to elasticsearch
func LoadToES(adresses <-chan AdressObject) {
	store, _ := NewStore()
	for object := range adresses {
		err := store.Create(&object)
		if err != nil {
			fmt.Println("Index create error - ", err)
		}
	}
}

// NewStore returns a new instance of the store.
//
func NewStore() (*Store, error) {
	cfg := elasticsearch.Config{
		Addresses: []string{
			"http://elasticsearch:9200",
		},
	}
	es, err := elasticsearch.NewClient(cfg)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	s := Store{es: es, indexName: "adress"}
	return &s, nil
}

// Create indexes a new document into store.
//
func (s *Store) Create(item *AdressObject) error {
	payload, err := json.Marshal(item)
	if err != nil {
		return err
	}

	ctx := context.Background()

	res, err := esapi.CreateRequest{
		Index:      s.indexName,
		DocumentID: item.ID,
		Body:       bytes.NewReader(payload),
	}.Do(ctx, s.es)

	defer res.Body.Close()

	if err != nil {
		return fmt.Errorf("Error creating request: %s", err)
	}

	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			return err
		}
		return fmt.Errorf("[%s] %s: %s", res.Status(), e["error"].(map[string]interface{})["type"], e["error"].(map[string]interface{})["reason"])
	}

	return nil
}
