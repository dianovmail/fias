package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"

	esapi "github.com/elastic/go-elasticsearch/v7/esapi"
)

type bulkResponse struct {
	Errors bool `json:"errors"`
	Items  []struct {
		Index struct {
			ID     string `json:"_id"`
			Result string `json:"result"`
			Status int    `json:"status"`
			Error  struct {
				Type   string `json:"type"`
				Reason string `json:"reason"`
				Cause  struct {
					Type   string `json:"type"`
					Reason string `json:"reason"`
				} `json:"caused_by"`
			} `json:"error"`
		} `json:"index"`
	} `json:"items"`
}

// M is an alias for map[string]interface{}
type M map[string]interface{}

// BulkLoadToES performs bulf insert in ES
func BulkLoadToES(adresses <-chan AdressObject) {
	var (
		buf bytes.Buffer
		res *esapi.Response
		err error

		numItems     int
		numErrors    int
		batchErrors  int
		numIndexed   int
		batchIndexed int
		currBatch    int
	)

	store, _ := NewStore()

	if res, err = store.es.Indices.Delete([]string{store.indexName}); err != nil {
		log.Fatalf("Cannot delete index: %s", err)
	}

	mapping := `{
		"mappings": {
			"properties": {
				"relation_type": {
					"type":                 "join",
					"eager_global_ordinals": true,
					"relations": {
						"city": "adress"
					}
				}
			}
		}
	}`

	res, err = store.es.Indices.Create(store.indexName, store.es.Indices.Create.WithBody(strings.NewReader(mapping)))

	if err != nil {
		log.Fatalf("Cannot create index: %s", err)
	}
	if res.IsError() {
		log.Fatalf("Cannot create index: %s", res)
	}

	start := time.Now().UTC()

	for adress := range adresses {
		numItems++

		meta := []byte(fmt.Sprintf(`{ "index" : { "_id" : "%s", "routing" : "%s" } }%s`, adress.GUID, adress.Parent, "\n"))

		data, err := json.Marshal(adress)
		if err != nil {
			log.Fatalf("Cannot encode adress %s: %s", adress.ID, err)
		}

		data = append(data, "\n"...)

		buf.Grow(len(meta) + len(data))
		buf.Write(meta)
		buf.Write(data)

		if buf.Len() > 10*1024*1024 {
			currBatch++
			log.Printf("> Batch %d with %d items", currBatch, numItems)
			batchErrors, batchIndexed, err = Write(store, &buf, numItems)
			if err != nil {
				log.Fatalf("Failure indexing batch %d: %s", currBatch, err)
			}
			numErrors += batchErrors
			numIndexed += batchIndexed
			// Reset the buffer and items counter
			//
			buf.Reset()
			numItems = 0
		}
	}
	if buf.Len() > 0 {
		currBatch++
		log.Printf("> Batch %d with %d items", currBatch, numItems)
		batchErrors, batchIndexed, err = Write(store, &buf, numItems)
		if err != nil {
			log.Fatalf("Failure indexing batch %d: %s", currBatch, err)
		}
		numErrors += batchErrors
		numIndexed += batchIndexed
		// Reset the buffer and items counter
		//
		buf.Reset()
		numItems = 0
	}

	log.Println(strings.Repeat("=", 80))

	dur := time.Since(start)

	if numErrors > 0 {
		log.Fatalf(
			"Indexed [%d] documents with [%d] errors in %s (%.0f docs/sec)",
			numIndexed,
			numErrors,
			dur.Truncate(time.Millisecond),
			1000.0/float64(dur/time.Millisecond)*float64(numIndexed),
		)
	} else {
		log.Printf(
			"Sucessfuly indexed [%d] documents in %s (%.0f docs/sec)",
			numIndexed,
			dur.Truncate(time.Millisecond),
			1000.0/float64(dur/time.Millisecond)*float64(numIndexed),
		)
	}
}

//Write writes to es
func Write(store *Store, buf *bytes.Buffer, numItems int) (int, int, error) {
	var (
		numErrors, numIndexed int
		raw                   map[string]interface{}
		blk                   *bulkResponse
	)
	res, err := store.es.Bulk(bytes.NewReader(buf.Bytes()), store.es.Bulk.WithIndex(store.indexName))
	if err != nil {
		return 0, 0, err
	}
	// If the whole request failed, print error and mark all documents as failed
	//
	if res.IsError() {
		numErrors += numItems
		if err := json.NewDecoder(res.Body).Decode(&raw); err != nil {
			log.Fatalf("Failure to to parse response body: %s", err)
		} else {
			log.Printf("  Error: [%d] %s: %s",
				res.StatusCode,
				raw["error"].(map[string]interface{})["type"],
				raw["error"].(map[string]interface{})["reason"],
			)
		}
		// A successful response might still contain errors for particular documents...
		//
	} else {
		if err := json.NewDecoder(res.Body).Decode(&blk); err != nil {
			log.Fatalf("Failure to to parse response body: %s", err)
		} else {
			for _, d := range blk.Items {
				// ... so for any HTTP status above 201 ...
				//
				if d.Index.Status > 201 {
					// ... increment the error counter ...
					//
					numErrors++

					// ... and print the response status and error information ...
					log.Printf("  Error: [%d]: %s: %s: %s: %s",
						d.Index.Status,
						d.Index.Error.Type,
						d.Index.Error.Reason,
						d.Index.Error.Cause.Type,
						d.Index.Error.Cause.Reason,
					)
				} else {
					// ... otherwise increase the success counter.
					//
					numIndexed++
				}
			}
		}
	}
	return numErrors, numIndexed, nil
}
