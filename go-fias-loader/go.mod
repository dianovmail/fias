module fias/go-fias-loader

require (
	github.com/elastic/go-elasticsearch v0.0.0
	github.com/elastic/go-elasticsearch/v7 v7.3.0
	github.com/fsnotify/fsnotify v1.4.7
)

go 1.12
